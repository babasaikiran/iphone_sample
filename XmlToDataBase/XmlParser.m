//
//  XmlParser.m
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "XmlParser.h"
#import "XmlItem.h"

@implementation XmlParser

@synthesize items;

-(NSMutableArray*) loadXMLByFile
{
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"sample.xml"];
    NSData *data = [NSData dataWithContentsOfFile: path];
    if(data) {
        parser = [[NSXMLParser alloc] initWithData:data];
        parser.delegate = self;
        [parser setShouldResolveExternalEntities:YES];
        if ( ! [parser parse] )
            NSLog(@"Parser error: %@", parser.parserError);
    }
    return items;
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementname isEqualToString:@"categories"]) 
    {
        items = [[NSMutableArray alloc] init];
        currentElementValue = [[NSMutableString alloc] init];
    }
    if ([elementname isEqualToString:@"category"])
    {
        pathCount++;
        currentItem = [[XmlItem alloc] init];
    }
    else if ([elementname isEqualToString:@"path"])
    {
        pathCount = 0;
    }
    else if ([elementname isEqualToString:@"subCategories"])
    {
        parent = [[NSString alloc] init];
        XmlItem *last = [items objectAtIndex:(items.count-1)];
        parent = last.itemID;
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{    
    if ([elementname isEqualToString:@"id"]) 
    {
        currentItem.itemID = currentElementValue;         
    }
    else if ([elementname isEqualToString:@"name"]) 
    {
        currentItem.name = currentElementValue;
    }
    else if ([elementname isEqualToString:@"category"])
    {
        if(currentItem.name != nil)
        {
            currentItem.parent = parent;
            [items addObject:currentItem];
            currentItem = nil;
        }
    }
    else if ([elementname isEqualToString:@"path"])
    {
        while(pathCount != 1)
        {
            XmlItem *item = [items objectAtIndex:(items.count - pathCount)];
            XmlItem *last = [items objectAtIndex:(items.count-pathCount+1)];
            last.parent = item.itemID;
            NSLog(@"parent is %@ child %@",item.name, last.name);
            [items replaceObjectAtIndex:(items.count - pathCount+1) withObject:last];
            pathCount--;
        }
    }
    else if ([elementname isEqualToString:@"subCategories"]) 
    {
        parent = nil;
    }
    
     currentElementValue = [[NSMutableString alloc] init];    
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [currentElementValue appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    NSLog(@"%@",currentElementValue);
}

@end

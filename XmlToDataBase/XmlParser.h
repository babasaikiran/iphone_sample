//
//  XmlParser.h
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XmlItem.h"

@interface XmlParser : NSObject <NSXMLParserDelegate> {
    
    NSMutableString *currentElementValue;
    NSMutableArray  *items;
    NSXMLParser     *parser;
    XmlItem         *currentItem;
    NSInteger        pathCount;
    NSString         *parent;
}

@property (nonatomic, retain) NSMutableArray *items;

-(id) loadXMLByFile;

@end

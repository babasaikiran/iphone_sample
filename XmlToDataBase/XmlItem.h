//
//  XmlItem.h
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlItem : NSObject
{
    NSString *name;
    NSString *itemID;
    NSString *parent;
}

@property (nonatomic, retain) NSString   *name;
@property (nonatomic, retain) NSString   *itemID;
@property (nonatomic, retain) NSString   *parent;

@end

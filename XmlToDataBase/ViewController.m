//
//  ViewController.m
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "sqlite3.h"
#import "XmlItem.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize selectedItem, objectAndCount, tables, viewController;

- (id)initWithTitle:(NSString *)title { 
    self = [super init];
    if(self)
    {
        self.title = NSLocalizedString(title, title);
    }
    return self;
}

- (void)loadView {
    [super loadView];
    [self loadDataBaseItems];
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) style:UITableViewStylePlain];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView reloadData];
    [self.view addSubview:tableView];    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return tables.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *myCell = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCell];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myCell];
    }
    
    XmlItem *item = [tables objectAtIndex:indexPath.row];
    NSNumber *num = [objectAndCount objectForKey:item.itemID];
    if([num intValue] != 0)
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else 
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    
    cell.textLabel.text = item.name;
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XmlItem *item = [tables objectAtIndex:indexPath.row];
    NSNumber *num = [objectAndCount objectForKey:item.itemID];
    if([num intValue] != 0)
    {
        self.viewController = [[ViewController alloc] initWithTitle:item.name];
        self.viewController.selectedItem = item.itemID;
        [self.navigationController pushViewController:self.viewController animated:YES];
    }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)loadDataBaseItems {
	sqlite3 *database;
    sqlite3_stmt *compiledStatement, *compiledStatementCount;
    
	tables = [[NSMutableArray alloc] init];
    objectAndCount = [[NSMutableDictionary alloc] init];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* dbFile = [documentsPath stringByAppendingPathComponent:@"Items.sql"];
    
	if(sqlite3_open([dbFile UTF8String], &database) == SQLITE_OK) {
		if(sqlite3_prepare_v2(database, [[NSString stringWithFormat:@"SELECT * FROM DATA WHERE PARENT='%@'", selectedItem] UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                XmlItem *item = [[XmlItem alloc] init];
				item.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                item.itemID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                item.parent = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                [tables addObject:item];
                if(sqlite3_prepare_v2(database, [[NSString stringWithFormat:@"SELECT COUNT(*) FROM DATA WHERE PARENT='%@'", item.itemID] UTF8String], -1, &compiledStatementCount, NULL) == SQLITE_OK) {
                    if(sqlite3_step(compiledStatementCount) == SQLITE_ROW) {
                        int result = (int)sqlite3_column_int(compiledStatementCount, 0);
                        [objectAndCount setObject:[NSNumber numberWithInt:result] forKey:(item.itemID)];                
                    }
                }
				sqlite3_finalize(compiledStatementCount);                
            }
        }
		sqlite3_finalize(compiledStatement);
	}
    else {
        NSLog(@"Failed to open database at MyDataBase.sql with error %s",  sqlite3_errmsg(database));
    }    
	sqlite3_close(database);
}

@end

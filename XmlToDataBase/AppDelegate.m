//
//  AppDelegate.m
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "XmlParser.h"
#import "ViewController.h"
#import "XmlItem.h"
#import "sqlite3.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if(![self checkForDatabase])
    {
        XmlParser *parser = [[XmlParser alloc] init];
        objects = [parser loadXMLByFile];
        [self creareAndPopulateDB];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[ViewController alloc] initWithTitle:@"Catalouge"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL) checkForDatabase {
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* dbFile = [documentsPath stringByAppendingPathComponent:@"Items.sql"];
    return [[NSFileManager defaultManager] fileExistsAtPath:dbFile];
}

- (void) creareAndPopulateDB {
    sqlite3 *database;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* dbFile = [documentsPath stringByAppendingPathComponent:@"Items.sql"];
    
	if(sqlite3_open([dbFile UTF8String], &database) == SQLITE_OK) {
        
        if (sqlite3_exec(database, [@"CREATE TABLE IF NOT EXISTS DATA (NAME TEXT, ITEMID TEXT PRIMARY KEY UNIQUE, PARENT TEXT)" UTF8String], NULL, NULL, NULL) == SQLITE_OK)
        {
            for( XmlItem *i in objects)
            {
                
                if (sqlite3_exec(database, [[NSString stringWithFormat:@"INSERT OR REPLACE INTO DATA (NAME, ITEMID, PARENT) VALUES ('%@','%@', '%@')", i.name, i.itemID, i.parent] UTF8String], NULL, NULL, NULL) != SQLITE_OK)
                {
                    NSLog(@"Error while inserting...");
                }                
            }
        }
    }
    else 
    {
        NSLog(@"Failed to open database at Items.sql with error %s",  sqlite3_errmsg(database));
    }    
	sqlite3_close(database);
}

@end

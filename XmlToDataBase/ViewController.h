//
//  ViewController.h
//  XmlToDataBase
//
//  Created by Sai Kiran on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *tables;
    NSMutableDictionary *objectAndCount;
}

@property (strong, nonatomic) NSString *selectedItem;
@property (nonatomic, retain) NSMutableDictionary *objectAndCount;
@property (strong, nonatomic) NSMutableArray *tables;
@property (strong, nonatomic) ViewController *viewController;

- (id)initWithTitle:(NSString *)title;
@end
